provider "aws" {
  region = "us-west-2"
  access_key = "AKIA4PRWVQTGW2FAH537"
  secret_key = "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"
}

variable vpc_cidr_blocks {}
variable subnet_cidr_blocks {}
variable avail_zone {}
variable env_prefix{}
variable my_ip{}
variable instance_type{}

resource "aws_vpc" "myapp-vpc" {
    cidr_block = var.vpc_cidr_blocks
    tags = {
      Name = "${var.env_prefix}-vpc"
    }
}

resource "aws_subnet" "myapp-subnet-1" {
    vpc_id = aws_vpc.myapp-vpc.id
    cidr_block = var.subnet_cidr_blocks
    availability_zone = var.avail_zone
    tags = {
       Name = "${var.env_prefix}-subnet-1"
    }
}

resource "aws_internet_gateway" "myapp_igw" {
  vpc_id = aws_vpc.myapp-vpc.id
  tags = {
    Name: "${var.env_prefix}-igw"
  }
}

resource "aws_default_route_table" "main_rtb" {
  default_route_table_id =  aws_vpc.myapp-vpc.default_route_table_id
  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.myapp_igw.id
  }
  tags = {
    Name: "${var.env_prefix}-rtb"
  }
}

/*resource "aws_route_table_association" "a-rtb-subnet" {
  subnet_id = aws_subnet.myapp-subnet-1.id
  route_table_id = aws_route_table.myapp-route-table.id  
}*/

resource "aws_default_security_group" "default-sg" {
  vpc_id = aws_vpc.myapp-vpc.id

  ingress {
    from_port = 22
    to_port = 22
    protocol = "tcp"
    cidr_blocks = [var.my_ip]
  }
  ingress {
    from_port = 8080
    to_port = 8080
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  egress {
    from_port = 0
    to_port = 0
    protocol = "-1"
    cidr_blocks = ["0.0.0.0/0"]
    prefix_list_ids = []
  }
  tags = {
    Name: "${var.env_prefix}-default-sg"
  }
}

data "aws_ami" "latest-amazon-linux-image"{
  most_recent = true
  owners = ["amazon"]
  filter {
    name = "name"
    values = ["amzn2-ami-hvm-*-x86_64-gp2"]
  }
}

output "aws_ami_id" {
  value = data.aws_ami.latest-amazon-linux-image.id
}

output "public_ip" {
  value = aws_instance.myapp-server.public_ip
}

resource "aws_instance" "myapp-server" {
  ami = data.aws_ami.latest-amazon-linux-image.id
  instance_type = var.instance_type
  
  subnet_id = aws_subnet.myapp-subnet-1.id
  vpc_security_group_ids = [aws_default_security_group.default-sg.id]
  availability_zone =  var.avail_zone

  user_data = file("entry-script.sh")

  associate_public_ip_address = true
  key_name = aws_key_pair.ssh-key.key_name
    tags = {
    Name: "${var.env_prefix}-server"
  }
}

resource "aws_key_pair" "ssh-key" {
  key_name = "server-key"
  public_key = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQDBoGPy5YpdTZ+N4YzN3/+aAjXJF3lgvHtwLoODNMT8wd/llYdz/I2VvKXSMAZFMxRDKp0/J7rnhT33u6jXgNxZGgw2ruJgSFgGQ+ERUMD3HFO4gTSKaik6SoGmL8Gudy2Z9EeMHhRBMOVfg8hk93BwmUmd2Qix9MvdbD5Frvmrwwl4/tuWpNQXPkaEdyZxKRk2xLsPzWKvI0gxvYAWABklUDA+sT7Y1WHGFCUdj27fQmKp6PtwAXW3SkVZKdbjK0JrMjLlML95VAYWSuKGQdJ5EsFMiVh5wo4bHDv7+gGRwtFuuthRYC7FmFPDC29HDz5WB+DDykl7tH/coYK44HvAZ8+8QmOwPUjj6xk68jN90q3LiW4Dj8Noyyul20vDGakWIRxAR6rQUG9LZsDArun6nRRFpAX4BlVK6sD0Q+CoU6ihvODDRqSNzGibqeUTZ0dvyH7pvkUkUFtVsDJQvsaxmMU7BvVxvLtOzJ0+ZbLO3uKx27KphqDaFBOvE4tsYis= peloton/uguumur.enkhjargal@LP-PCE-598"
}


